#!/usr/bin/env python3

import paho.mqtt.client as mqtt
import threading
import random


aluhut_level = 0.1


def on_connect(client, userdata, flags, rc):
    client.subscribe('hq/aluhut/#')


def on_message(client, userdata, msg):
    global aluhut_level
    mqttmsg = msg.payload.decode()
    if msg.topic == 'hq/aluhut':
        pass
    elif msg.topic == 'hq/aluhut/alarm':
        aluhut_level = 42
        client.publish('hq/aluhut', payload=42, retain=True)


def generate_level(current_level):
    if current_level > 1:
        new_level = random.uniform(current_level - 1, current_level)
        return new_level
    else:
        new_level = random.uniform(0.01, 0.99)
        return new_level


def main_thread():
    global aluhut_level
    aluhut_level = generate_level(aluhut_level)
    client.publish('hq/aluhut', payload=f'{round(aluhut_level, 2)}', retain=True)
    t = threading.Timer(30.0, main_thread)
    t.start()


client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message

client.connect('mqtt.cccffm.space')

if __name__ == '__main__':
    main_thread()
    client.loop_forever()
