FROM python:3.7-alpine



RUN apk add --no-cache git && \
        mkdir -p /srv/ && \
        cd /srv && \
        git clone https://chaos.expert/telegnom/aluhutalarm.git && \
        pip install -r /srv/aluhutalarm/requirements.txt && \
        chmod u+x /srv/aluhutalarm/src/aluhut.py

CMD ["python", "/srv/aluhutalarm/src/aluhut.py"]

